FROM python:3-alpine

ENTRYPOINT ["/usr/local/bin/curator"]

RUN adduser -u 1000 -G root -D app &&\
  pip install -q --upgrade --no-cache-dir elasticsearch-curator==5.8.4 elasticsearch==7.13.4 &&\
  curator --version

USER 1000

ARG IMAGE_COMMIT_ARG=""
ENV IMAGE_COMMIT=${IMAGE_COMMIT_ARG}
ARG IMAGE_TAG_ARG=""
ENV IMAGE_TAG=${IMAGE_TAG_ARG}
